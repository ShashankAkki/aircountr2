package com.aircountr.android;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by VOJJALA TEJA on 14-06-2016.
 */
public class GoogleAnalyticsTracker
{
    public enum Target {
        APP_TRACKER,
    }

    private static GoogleAnalyticsTracker sInstance;

    public static synchronized void init(Context context) {
        if (sInstance != null)
        {
            throw new IllegalStateException("Tracker already initiated");
        }
        sInstance = new GoogleAnalyticsTracker(context);
    }

    public static synchronized GoogleAnalyticsTracker getInstance()
    {
        if (sInstance == null)
        {
            throw new IllegalStateException("Tracker not initialised,call init() before getInstance()");
        }
        return sInstance;
    }

    private final Map<Target, Tracker> mTrackers = new HashMap<Target, Tracker>();
    private final Context mContext;
    private GoogleAnalyticsTracker(Context context) {
        mContext = context.getApplicationContext();
    }
    public synchronized Tracker get(Target target) {
        if (!mTrackers.containsKey(target)) {
            Tracker tracker;
            switch (target) {
                case APP_TRACKER:
                    tracker = GoogleAnalytics.getInstance(mContext).newTracker(R.xml.googleanalyticstracker);
                    break;
                default:
                    throw new IllegalArgumentException("Unhandled analytics target " + target);
            }
            mTrackers.put(target, tracker);
        }
        return mTrackers.get(target);
    }
}