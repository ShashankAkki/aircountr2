package com.aircountr.android.model;

/**
 * Created by VikramV on 6/12/2016.
 */
public class Category {
    private String categoryName;
    private String categoryURL;
    private boolean isSelected;

    public Category() {
    }

    public Category(String categoryName, String categoryURL, boolean isSelected) {
        this.categoryName = categoryName;
        this.categoryURL = categoryURL;
        this.isSelected = isSelected;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryURL() {
        return categoryURL;
    }

    public void setCategoryURL(String categoryURL) {
        this.categoryURL = categoryURL;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}