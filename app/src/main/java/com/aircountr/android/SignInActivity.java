package com.aircountr.android;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.prefrences.AppPreferences;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.splunk.mint.Mint;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.smooch.core.User;


/**
 * Created by gaurav on 4/10/2016.
 */
public class SignInActivity extends BaseActivity implements View.OnClickListener, TextView.OnEditorActionListener {
    private String TAG = this.getClass().getSimpleName();
    private EditText et_email;
    private EditText et_password;
    private TextView tv_forgotPassword;
    private TextView tv_signIn;
    private TextView tv_or;
    private TextView tv_newUser;
    private TextView tv_signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(SignInActivity.this, "058c75a4");

        setContentView(R.layout.activity_sign_in);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        tv_forgotPassword = (TextView) findViewById(R.id.tv_forgotPassword);
        tv_signIn = (TextView) findViewById(R.id.tv_signIn);
        tv_or = (TextView) findViewById(R.id.tv_or);
        tv_newUser = (TextView) findViewById(R.id.tv_newUser);
        tv_signUp = (TextView) findViewById(R.id.tv_signUp);

        et_email.setTypeface(REGULAR);
        et_password.setTypeface(REGULAR);
        tv_forgotPassword.setTypeface(REGULAR);
        tv_signIn.setTypeface(REGULAR);
        tv_or.setTypeface(REGULAR);
        tv_newUser.setTypeface(REGULAR);
        tv_signUp.setTypeface(REGULAR);

        et_email.setText(AppPreferences.getPhoneNo(getApplicationContext()));
        et_password.setText(AppPreferences.getPassword(getApplicationContext()));

        tv_signUp.setOnClickListener(this);
        tv_signIn.setOnClickListener(this);

        et_email.setOnEditorActionListener(this);
        et_password.setOnEditorActionListener(this);

        tv_forgotPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   switchActivity(getApplicationContext(), ForgotPasswordActivity.class);
                }
        });
        if(getIntent().getBooleanExtra("newRegistration", false)){
            tv_signIn.performClick();
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
            tv_signIn.performClick();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.tv_signUp: {
                switchActivity(SignInActivity.this, RegistrationActivity.class);
            }
            break;
            case R.id.tv_signIn:
                if(!(et_email.getText().toString().equals("")||et_password.getText().toString().equals(""))) {
                    authenticateUser(et_email.getText().toString(), et_password.getText().toString());
                }
                else{
                    Toast.makeText(getBaseContext(),"Please complete your credentials",Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void authenticateUser(final String email, final String password) {
        if (isNetworkAvailable()) {
            // Tag used to cancel the request
            String tag_json_obj = "json_obj_authenticate";
            final String url = CreateUrl.getLoginUrl();
            Map<String, String> params = new HashMap<String, String>();
            params.put("mobile", email);
            params.put("password", password);

            final ProgressDialog progessDialog = new ProgressDialog(this);
            progessDialog.setMessage("Authenticating...");
            progessDialog.show();

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("hello", response.toString());
                    try {
                        Boolean status = response.getBoolean("success");
                        if (status) {
                            AppPreferences.setAccessToken(SignInActivity.this, response.getString("token"));
                            AppPreferences.setMerchantId(SignInActivity.this, response.getString("Merchantid"));
//                            User.getCurrentUser().setFirstName(response.getString("businessname"));
                            User.getCurrentUser().setEmail(response.getString("email"));
                            AppPreferences.setBusinessName(SignInActivity.this, response.getString("businessname"));
                            AppPreferences.setEmailId(SignInActivity.this, response.getString("email"));
                            AppPreferences.setPhoneNo(SignInActivity.this, response.getString("mobile"));
                            AppPreferences.setOperationalHourFrom(SignInActivity.this, response.getString("openTime"));
                            AppPreferences.setOperationalHourTo(SignInActivity.this, response.getString("closeTime"));
                            AppPreferences.setNoOfEmployee(SignInActivity.this, response.getString("noofemployees"));
                            AppPreferences.setPassword(SignInActivity.this, et_password.getText().toString());
                            AppPreferences.setRefferalCode(SignInActivity.this, response.getString("referralCode"));
                            switchActivity(SignInActivity.this, HomeActivity.class);
                            finish();
                            Log.i("RAHUL", "Login Success");
                        } else {
                            displayToast("Your Username/Password Is Wrong");
                        }
                    } catch (JSONException e) {
                        AircountrApplication.getInstance().trackException(e);
                        e.printStackTrace();
                    }
                    progessDialog.hide();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    displayToast("API not responding");
                    // hide the progress dialog
                    progessDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile", email);
                    params.put("password", password);
                    return params;
                }

                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };
            // Adding request to request queue
            AircountrApplication.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);
        } else {
            showDialog("Error", getResources().getString(R.string.no_internet), "OK");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        et_email.setText(AppPreferences.getPhoneNo(getApplicationContext()));
        et_password.setText(AppPreferences.getPassword(getApplicationContext()));
        AircountrApplication.getInstance().trackScreenView(TAG);
    }

    @Override
    public void onBackPressed() {
//        System.exit(0);
        finish();
    }
}