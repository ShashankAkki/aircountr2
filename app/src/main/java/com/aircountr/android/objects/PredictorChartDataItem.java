package com.aircountr.android.objects;

/**
 * Created by VOJJALA TEJA on 19-06-2016.
 */
public class PredictorChartDataItem {

    int actualamount;
    int predictedamount;

    public int getActualamount() {
        return actualamount;
    }

    public void setActualamount(int actualamount) {
        this.actualamount = actualamount;
    }

    public int getPredictedamount() {
        return predictedamount;
    }

    public void setPredictedamount(int predictedamount) {
        this.predictedamount = predictedamount;
    }

}
