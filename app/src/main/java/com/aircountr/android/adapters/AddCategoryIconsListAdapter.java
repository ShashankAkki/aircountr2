package com.aircountr.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.aircountr.android.AircountrApplication;
import com.aircountr.android.R;
import com.aircountr.android.model.Category;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;


/**
 * Created by gaurav on 4/27/2016.
 */
public class AddCategoryIconsListAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Category> mDataList;
    ImageLoader imageLoader = AircountrApplication.getInstance().getImageLoader();

    public AddCategoryIconsListAdapter(Context mContext, ArrayList<Category> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void onDataSetChanged(ArrayList<Category> mDataList) {
        this.mDataList = mDataList;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (mLayoutInflater == null)
            mLayoutInflater = (LayoutInflater) mContext.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.column_category_icons_grid, null);
            if (imageLoader == null)
                imageLoader = AircountrApplication.getInstance().getImageLoader();

            viewHolder.iconImg = (NetworkImageView) convertView.findViewById(R.id.iv_iconImg);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.iconImg.setImageUrl(mDataList.get(position).getCategoryURL(), imageLoader);
        if (mDataList.get(position).isSelected()) {
            convertView.setBackgroundResource(R.drawable.shape_rect_blue_border_with_transparent_bg);
        } else {
            convertView.setBackgroundResource(R.drawable.shape_rect_blue_border_category);
        }
        return convertView;
    }

    private class ViewHolder {
        NetworkImageView iconImg;
    }
}