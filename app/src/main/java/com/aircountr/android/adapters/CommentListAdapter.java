package com.aircountr.android.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aircountr.android.R;
import com.aircountr.android.objects.InvoiceListItem;

import java.util.ArrayList;

/**
 * Created by Saahil on 16/06/16.
 */
public class CommentListAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<InvoiceListItem> mDataList;
    private Typeface REGULAR, SEMIBOLD;

    public CommentListAdapter(Context mContext, ArrayList<InvoiceListItem> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Semibold.ttf");
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void onDataSetChanged(ArrayList<InvoiceListItem> mDataList) {
        this.mDataList = mDataList;
        notifyDataSetChanged();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.row_commentlist, null);

        TextView tv_vendorName = (TextView) convertView.findViewById(R.id.tv_vendorNamecom);
        TextView tv_comment = (TextView) convertView.findViewById(R.id.tv_commentcom);

        tv_vendorName.setTypeface(SEMIBOLD);
        tv_comment.setTypeface(REGULAR);

        tv_vendorName.setText(mDataList.get(position).getVendorName());
        tv_comment.setText(mDataList.get(position).getComment());


        return convertView;
    }
}
