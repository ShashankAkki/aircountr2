package com.aircountr.android.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aircountr.android.AircountrApplication;
import com.aircountr.android.R;
import com.aircountr.android.objects.CategoryListItem;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;


/**
 * Created by gaurav on 4/26/2016.
 */
public class CategoryListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<CategoryListItem> mDataList;
    private LayoutInflater mLayoutInflater;
    private Typeface SEMIBOLD;
    ImageLoader imageLoader = AircountrApplication.getInstance().getImageLoader();

    public CategoryListAdapter(Context mContext, ArrayList<CategoryListItem> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.SEMIBOLD = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Semibold.ttf");
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void onDataSetChanged(ArrayList<CategoryListItem> mDataList) {
        this.mDataList = mDataList;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (mLayoutInflater == null)
            mLayoutInflater = (LayoutInflater) mContext.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.column_category_grid, null);
            if (imageLoader == null)
                imageLoader = AircountrApplication.getInstance().getImageLoader();

            viewHolder.iv_categoryImg = (NetworkImageView) convertView.findViewById(R.id.iv_categoryImg);
            viewHolder.tv_categoryName = (TextView) convertView.findViewById(R.id.tv_categoryName);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_categoryName.setText(mDataList.get(position).getCategoryName());
        viewHolder.tv_categoryName.setTypeface(SEMIBOLD);
        viewHolder.iv_categoryImg.setImageUrl(mDataList.get(position).getResourceId(), imageLoader);

        return convertView;
    }

    private class ViewHolder {
        NetworkImageView iv_categoryImg;
        TextView tv_categoryName;
    }
}