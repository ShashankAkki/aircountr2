package com.aircountr.android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aircountr.android.HomeActivity;
import com.aircountr.android.R;
import com.aircountr.android.SignInActivity;
import com.aircountr.android.adapters.MyInvoicesAdapter;
import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.objects.CategoryListItem;
import com.aircountr.android.objects.InvoiceListItem;
import com.aircountr.android.objects.PreCachingLayoutManager;
import com.aircountr.android.prefrences.AppPreferences;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

public class MyInvoicesFragment extends Fragment {

    private String TAG = this.getClass().getSimpleName();
    private RecyclerView rv_myInvoices;
    public ArrayList<InvoiceListItem> mInvoiceListitems = new ArrayList<>();
    private InvoiceListItem rowData;
    private TextView tv_msgText;
    public static boolean initialised = false;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<CategoryListItem> categoryListItems;
    private MyInvoicesAsync task=null;
    ArrayList<InvoiceListItem> searchList = new ArrayList<InvoiceListItem>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_invoices,null);
        rv_myInvoices = (RecyclerView)root.findViewById(R.id.rv_myInvoices);
        tv_msgText = (TextView)root.findViewById(R.id.tv_msgText);
        tv_msgText.setTypeface(((HomeActivity)getActivity()).REGULAR);
        swipeRefreshLayout = (SwipeRefreshLayout)root.findViewById(R.id.swipeRefreshLayout);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initialised = false;
                if (task != null && task.getStatus() == AsyncTask.Status.RUNNING) {
                    task.cancel(true);
                }
                sendMyInvoicesRequest();
            }
        });
        sendMyInvoicesRequest();
        ((HomeActivity) getActivity()).et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String strText = s.toString().trim();
                if (strText.length() > 0) {
                    searchList.clear();
                    for (int i = 0; i < mInvoiceListitems.size(); i++) {
                        if (mInvoiceListitems.get(i).getCategoryName().toLowerCase().contains(strText.toLowerCase())||mInvoiceListitems.get(i).getVendorName().toLowerCase().contains(strText.toLowerCase())) {
                            rowData = new InvoiceListItem();
                            rowData = mInvoiceListitems.get(i);
                            searchList.add(rowData);
                        }
                    }
                    if (searchList.size() > 0) {
                        tv_msgText.setVisibility(View.GONE);
                        rv_myInvoices.setAdapter(new MyInvoicesAdapter(getContext(),searchList));
                    } else {
                        tv_msgText.setVisibility(View.VISIBLE);
                    }
                } else {
                    rv_myInvoices.setAdapter(new MyInvoicesAdapter(getContext(), mInvoiceListitems));
                    tv_msgText.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setData() {
        initialised = true;
        rv_myInvoices.setLayoutManager(new PreCachingLayoutManager(getContext()));
        Collections.sort(mInvoiceListitems, new Comparator<InvoiceListItem>() {
            @Override
            public int compare(InvoiceListItem lhs, InvoiceListItem rhs) {
                String date = lhs.getCreatedDate();
                Calendar calendar = Calendar.getInstance();
                calendar.set(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(5, 7)), Integer.parseInt(date.substring(8, 10)));
                long seconds = calendar.getTimeInMillis();
                date = rhs.getCreatedDate();
                calendar.set(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(5, 7)), Integer.parseInt(date.substring(8, 10)));
                long seconds2 = calendar.getTimeInMillis();
                if (seconds2 < seconds)
                    return -1;
                else
                    return 1;
                //return Long.compare(seconds2, seconds);
            }
        });
        rv_myInvoices.setAdapter(new MyInvoicesAdapter(getContext(), mInvoiceListitems));
    }

    private void sendMyInvoicesRequest() {
        if (((HomeActivity) getActivity()).isNetworkAvailable()) {
            if (!AppPreferences.getMerchantId(getActivity()).equals("") && !AppPreferences.getAccessToken(getActivity()).equals("")) {
                Log.d(TAG, AppPreferences.getAccessToken(getContext()));
                task = new MyInvoicesAsync();
                task.execute(AppPreferences.getMerchantId(getActivity()), AppPreferences.getAccessToken(getActivity()));
            } else {
                ((HomeActivity) getActivity()).switchActivity(getActivity(), SignInActivity.class);
            }
        } else {
            ((HomeActivity) getActivity()).showDialog("Error", getActivity().getResources().getString(R.string.no_internet), "OK");
        }

    }

    private class MyInvoicesAsync extends AsyncTask<String, Void, String> {

        private JSONObject invoiceObj;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!initialised)
                ((HomeActivity) getActivity()).showLoading("Loading...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {
                Log.d(TAG, s);
                try {
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    if (success) {
                        JSONArray invoiceArray = response.getJSONArray("invoice");
                        if (invoiceArray != null && invoiceArray.length() > 0) {
                            if (!initialised)
                                mInvoiceListitems.clear();
                            for (int i = 0; i < invoiceArray.length(); i++) {
                                invoiceObj = invoiceArray.getJSONObject(i);
                                rowData = new InvoiceListItem();
                                Log.d("hello", invoiceObj.toString());
                                try {
                                    String vendorId = getRowData("vendorId");
                                    String invoiceAmount = getRowData("invoiceamount");
                                    String comments = getRowData("comments");
                                    String vendorName = getRowData("vendorname");
                                    String categoryName = getRowData("categoryname");
                                    String date = getRowData("createdDate");
                                    String invoiceId = getRowData("_id");
                                    String categoryId = getRowData("categoryId");
                                    String imageUrl = getRowData("imageurl");
                                    String isautomode = getRowData("isautomode");
                                    boolean isProcessed;
                                    try {
                                        isProcessed = invoiceObj.getBoolean("isprocessed");
                                    } catch (Exception e) {
                                        isProcessed = false;
                                    }
                                    rowData.setInvoiceId(invoiceId);
                                    rowData.setCategoryId(categoryId);
                                    rowData.setVendorId(vendorId);
                                    rowData.setInvoiceAmount(getContext().getResources().getString(R.string.txt_rupee) + invoiceAmount);
                                    rowData.setComment(comments);
                                    rowData.setVendorName(vendorName);
                                    rowData.setCategoryName(categoryName);
                                    rowData.setImageUrl(imageUrl);
                                    rowData.setIsProcessed(isProcessed);
                                    rowData.setCreatedDate(date.substring(0, 10));
                                    rowData.setAutoMode(Boolean.parseBoolean(isautomode));
                                } catch (Exception e) {

                                }
                                if (mInvoiceListitems.size() <= i)
                                    mInvoiceListitems.add(rowData);
                                else if ((rowData.isProcessed() != mInvoiceListitems.get(i).isProcessed()) || ((rowData.getImageUrl() != mInvoiceListitems.get(i).getImageUrl())) || (rowData.getCategoryName() != mInvoiceListitems.get(i).getCategoryName()) || (rowData.getVendorId() != mInvoiceListitems.get(i).getVendorId()) || (rowData.getInvoiceAmount() != mInvoiceListitems.get(i).getInvoiceAmount()) || (rowData.getComment() != mInvoiceListitems.get(i).getComment()) || (rowData.getCreatedDate() != mInvoiceListitems.get(i).getCreatedDate())) {
                                    mInvoiceListitems.get(i).setInvoiceId(rowData.getInvoiceId());
                                    mInvoiceListitems.get(i).setCategoryId(rowData.getCategoryId());
                                    mInvoiceListitems.get(i).setVendorId(rowData.getVendorId());
                                    mInvoiceListitems.get(i).setInvoiceAmount(rowData.getInvoiceAmount());
                                    mInvoiceListitems.get(i).setComment(rowData.getComment());
                                    mInvoiceListitems.get(i).setVendorName(rowData.getVendorName());
                                    mInvoiceListitems.get(i).setCategoryName(rowData.getCategoryName());
                                    mInvoiceListitems.get(i).setImageUrl(rowData.getImageUrl());
                                    mInvoiceListitems.get(i).setIsProcessed(rowData.isProcessed());
                                    mInvoiceListitems.get(i).setCreatedDate(rowData.getCreatedDate());
                                    if (rv_myInvoices != null && rv_myInvoices.getAdapter() != null && rv_myInvoices.getAdapter().getItemCount() > i)
                                        rv_myInvoices.getAdapter().notifyItemChanged(i);
                                }
                            }
                            tv_msgText.setVisibility(View.GONE);
                        } else {
                            tv_msgText.setVisibility(View.VISIBLE);
                        }
                    } else {
                        String msg = response.getString("msg");
                        tv_msgText.setVisibility(View.VISIBLE);
                        tv_msgText.setText(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    ((HomeActivity) getActivity()).hideLoading();
                }
            } else {
                tv_msgText.setVisibility(View.VISIBLE);
            }
            ((HomeActivity) getActivity()).hideLoading();
            if (!initialised)
            {
                setData();
//                ((HomeActivity) getActivity()).hideLoading();
            }
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        }

        private String getRowData(String tag) {
            try {
                tag = invoiceObj.getString(tag);
            } catch (Exception e) {
                tag = "";
            } finally {
                return tag;
            }
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.invoiceListUrl(params[0]);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", params[1]);
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            if (resultCode == Activity.RESULT_OK) {
                int position = data.getIntExtra("position", 0);
                mInvoiceListitems.get(position).setInvoiceId(data.getStringExtra("invoiceId"));
                mInvoiceListitems.get(position).setCategoryId(data.getStringExtra("categoryId"));
                mInvoiceListitems.get(position).setVendorId(data.getStringExtra("vendorId"));
                mInvoiceListitems.get(position).setInvoiceAmount(data.getStringExtra("invoiceamount"));
                mInvoiceListitems.get(position).setComment(data.getStringExtra("comments"));
                mInvoiceListitems.get(position).setVendorName(data.getStringExtra("vendorname"));
                mInvoiceListitems.get(position).setCategoryName(data.getStringExtra("categoryname"));
                mInvoiceListitems.get(position).setImageUrl(data.getStringExtra("imageurl"));
                mInvoiceListitems.get(position).setIsProcessed(data.getBooleanExtra("isprocessed", true));
                mInvoiceListitems.get(position).setCreatedDate(data.getStringExtra("createdDate"));
                mInvoiceListitems.get(position).setAutoMode(data.getBooleanExtra("isautomode",false));
                //              rv_myInvoices.getAdapter().notifyItemChanged(position);
//                setData();
                initialised = false;
                sendMyInvoicesRequest();
            }
        }
    }
}
